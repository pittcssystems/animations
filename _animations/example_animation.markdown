---
layout: lab
title: Memory library
subtitle: Interact with a library as if you are a process interacting with the OS
summary: >-
  Example of an interactive application to model page management as a library.
  Developed by Students (https://github.com/sim-50/cs-tools)
released: 11:59 PM Monday, February 1st, 2021.
due: 11:59 PM Sunday, February 7th, 2021.
---


<iframe	src="https://sim-50.github.io/cs-tools/" style="width:100%; height:100%;"> 
</iframe>

