---
layout: page
title: Animations
permalink: /animations/
---

On this page, you will see our animations.

{% for animation in site.animations %}

{% unless animation.ignore or animation.ignore == true %}


# [{{ animation.title }}: {{ animation.subtitle}}]({{ site.baseurl}}{{ animation.url }})

{{ animation.summary }}

{% endunless %}

{% endfor %}
